#!/usr/bin/env python

import roslib; roslib.load_manifest('rosbag')
import rosbag, rospy, os

import datetime, shutil, fileinput
from argparse import ArgumentParser
from numpy import mean, array, hypot, diff, convolve, arange, sin, cos, ones, pi
from matplotlib.pyplot import *
from matplotlib.font_manager import FontProperties

# Plots are optional
try:
  from matplotlib import pyplot
except ImportError:
  pyplot = None

parser = ArgumentParser(description='Show scatter plot of radio RSSI against distance from ENU datum.')
parser.add_argument('bag', metavar='FILE', type=str, help='input bag file')
args = parser.parse_args()
bag = rosbag.Bag(args.bag)

filename = sys.argv[1]


#cmd_drive
left = []
right = []

#imu/compass_heading
imu_data = []

#imu/data
orientation_x = []
orientation_y = []
orientation_z = []
orientation_w = []

#orientation_covariance = [8][]

angular_velocity_x = []
angular_velocity_y = []
angular_velocity_z = []

#angular_velocity_covariance = [8][]

linear_x = []
linear_y = []
linear_z = []

#linear_acceleration_covariance = [8][]

#imu/temperature(/data)

imu_temperature = []

#navsat/rx(/sentence)
gps = []
#navsat/vel
velocity = []

#sense
battery = []
current_left = []
current_right = []
pcb_temperature = []

#/eff_wrench/(torque,/force)
force_x = []
force_y = []
force_z = []
torque_x = []
torque_y = []
torque_z = []

imu_data_times = []
imu_compass_times = []
cmd_drive_times = []
imu_temp_times = []
navsat_rx_times = []
navsat_vel_times = []
sense_times = []
eff_wrench_times = []

first_time = None

serial = 0
last_dist = 0

for topic, msg, time in bag.read_messages(topics=("/cmd_drive","/imu/compass_heading","/imu/data","/imu/temperature","/navsat/rx","/navsat/vel","/sense","/eff_wrench")):
  if not first_time: first_time = time
  if topic == "/imu/data":
	imu_data_times.append((time - first_time).to_sec())
	orientation_x.append(msg.orientation.x)
	orientation_y.append(msg.orientation.y)
	orientation_z.append(msg.orientation.z)
	orientation_w.append(msg.orientation.w)
	angular_velocity_x.append(msg.angular_velocity.x)
	angular_velocity_y.append(msg.angular_velocity.y)
	angular_velocity_z.append(msg.angular_velocity.z)
	linear_x.append(msg.linear_acceleration.x)
	linear_y.append(msg.linear_acceleration.y)
	linear_z.append(msg.linear_acceleration.z)
  if topic == "/imu/compass_heading":
    imu_compass_times.append((time - first_time).to_sec())
    imu_data.append(msg.data)
  if topic == "/cmd_drive":
    cmd_drive_times.append((time - first_time).to_sec())
    left.append(msg.left)
    right.append(msg.right)
  if topic == "/imu/temperature":
    imu_temp_times.append((time - first_time).to_sec())
    imu_temperature.append(msg.data)
  if topic == "/navsat/rx":
    navsat_rx_times.append((time - first_time).to_sec())
    gps.append(msg.sentence)
    #------Need to find a plugin to save an image of a map of the path---------
  if topic == "/navsat/vel":
    navsat_vel_times.append((time - first_time).to_sec())
    velocity.append(msg.data)
  if topic == "/sense":
    sense_times.append((time - first_time).to_sec())
    battery.append(msg.battery)
    current_left.append(msg.current_left)
    current_right.append(msg.current_right)
    pcb_temperature.append(msg.pcb_temperature)
  if topic == "/eff_wrench":
    eff_wrench_times.append((time - first_time).to_sec())
    force_x.append(msg.force.x)
    force_y.append(msg.force.y)
    force_z.append(msg.force.z)
    torque_x.append(msg.torque.x)
    torque_y.append(msg.torque.y)
    torque_z.append(msg.torque.z)
	
loc = filename.index('_')

project = filename[:loc] #% loc
loc += 1
datetime = filename[loc:]
#print datetime
path = './%s' % project
#os.mkdir husky-%d, serial
#os.mkdir(path)
#topics(mcu_voltage, left_voltage, right_voltage, 'MCU', 'Left Motor', 'Right Motor', 'Voltage');
#topics(mcu_current, left_current, right_current, 'MCU', 'Left Motor', 'Right Motor', 'Current');
#4topics(left_jag_temp, right_jag_temp, left_motor_temp, right_motor_temp, 'Left Jag', 'Right Jag', 'Left Motor', 'Right Motor', 'Temperature');

#voltage_times = times
#current_times = times
#temperature_times = times

#times = times[150:-150]

if not os.path.exists(path):
        os.mkdir(path)

def strip_trim(s):
	if s.endswith(".bag"): s = s[:-4]
	if s.startswith("%s_"): s = s[loc:] % project
	return s

def generate_plots(data_sets, labels, plot_label, times):
	global serial#, times
	
	window_len=80
	trim = 10
	#times = times[trim:-trim]
	data_sets_smoothed = []
	w = ones(window_len, 'd')
	#for data_set in data_sets:
                #print len(data_sets_smoothed[-1])
		#data_sets_smoothed.append(convolve(w / w.sum(), data_set, mode='same')[trim:-trim])

	fontP = FontProperties()
	fontP.set_size('small')
	

	if pyplot:
		colours = ['green', 'red', 'yellow', 'blue', 'purple', 'black', 'orange', 'pink', 'grey']
		lines = []
		fig = pyplot.figure(figsize=(24, 6))  # 24x6 inches
		for data_set, colour, label in zip(data_sets, colours, labels):
			ax = fig.add_subplot(1, 1.8, 1)
			ax.set_color_cycle(colour)
			line, = ax.plot(times, data_set)
			lines.append(line)
			box = ax.get_position()
			ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
			label = label + ' ' + plot_label
		
		location = '%s/%s/' %(project, datetime)
		if not os.path.exists(location):
        		os.mkdir(location)
		lgd = ax.legend(lines, labels, 'right', bbox_to_anchor=(0, 0, 1, 1), prop=fontP, borderaxespad=-15)
		fig.savefig('%s/%s/%s' % (project, datetime, plot_label), bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=200)


datetime = strip_trim(args.bag)

bagfile = './' + args.bag

#datetime = '/' + datetime

badfile = './bad-bags'

if not os.path.exists(badfile):
        os.mkdir(badfile)

qcpath = path + '/QC Pictures'
testpath = path + '/Testing Data'
out = 0


try:
	generate_plots([orientation_x, orientation_y, orientation_z, orientation_w],
	               ['X', 'Y', 'Z', 'W'], '1 - IMU Orientation', imu_data_times)
        out += 1 #1
	generate_plots([angular_velocity_x, angular_velocity_y, angular_velocity_z], 
	               ['X', 'Y', 'Z'], '2 - IMU Angular Velocity', imu_data_times)
        out += 1 #2
	generate_plots([linear_x, linear_y, linear_z], 
	               ['X', 'Y', 'Z'], '3 - IMU Linear Acceleration', imu_data_times)
        out += 1 #3
	generate_plots([imu_data], 
	               ['Direction'], '4 - IMU Compass Heading', imu_compass_times)
        out += 1 #4
	generate_plots([left, right], 
	               ['Left', 'Right'], '5 - Drive command to thrusters', cmd_drive_times)
        out += 1 #5
	generate_plots([pcb_temperature], 
	               ['MCU temperature'], '6 - MCU Temperature', sense_times)
        out += 1 #6
   	generate_plots([imu_temperature], 
	               ['IMU temperature'], '7 - IMU Temperature', imu_temp_times)
        out += 1 #7
        #----------- 8 need to be fixed  VVVVVVVVVVVVVVVV  current file doesn't have /navsat/vel
	generate_plots([velocity], 
	               ['Velocity'], '8 - GPS Velocity', navsat_vel_times)
        out += 1 #8
        #----------- end ^^^^^^^^^^^^^^^^^^^^^^ 
	generate_plots([battery], 
	               ['Battery'], '9 - Voltage', sense_times)
        out += 1 #9
	generate_plots([current_left, current_right], 
	               ['Left thruster', 'Right thruster'], '10 - Current', sense_times)
        out += 1 #10
        #----------- also needs to be fixed  VVVVVVVVVVVVVV  --- /eff_wrench not in testing bag
	generate_plots([force_x, force_y, force_z], 
	               ['X', 'Y', 'Z'], '11 - Force', eff_wrench_times)
        out += 1 #11
	generate_plots([torque_x, torque_y, torque_z], 
	               ['X', 'Y', 'Z'], '12 - Torque', eff_wrench_times)
        out += 1 #12
        #----------- end ^^^^^^^^^^^^^^^^^^^^
	if not os.path.exists(path + '/' + datetime):
        	os.mkdir(path + '/' + datetime)

	if not os.path.exists(qcpath):
        	os.mkdir(qcpath)

	if not os.path.exists(testpath):
        	os.mkdir(testpath)

	shutil.move(bagfile,testpath)

except ValueError, e:
    print "Error: %s" % str(e)
    print "Line: %d" % out
    #print imu_data
#	shutil.move(bagfile,badfile)

#folderdata = './' + path
#dest = '/media/selina/husky testing'

#shutil.move(folderdata,dest)
